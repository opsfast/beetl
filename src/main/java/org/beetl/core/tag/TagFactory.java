package org.beetl.core.tag;

/** 标签函数工厂类
 * @author joelli
 *
 */
public interface TagFactory
{
	public Tag createTag();
}