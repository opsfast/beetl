package org.beetl.core.text;

public interface AttributeNameConvert {
    public String convert(String orginal);
}
